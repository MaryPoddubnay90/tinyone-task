$(document).ready(function(){
    $(window).on('load scroll resize', function() {
        var viewportWidth = $(window).width();
        if ($(this).scrollTop() > 1 || (viewportWidth < 767)){
            $('.header').addClass("sticky");
        }
        else{
            $('.header').removeClass("sticky");
        }
    });

    $(".main-nav li").find("a").click(function(e) {
        e.preventDefault();
        var section = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section).offset().top
        });
    });
});